#!/bin/sh
# This file is in the public domain.
# It extracts translateable strings from the JSON file(s).
#
# The resulting 'extracted.h' file is only used by
# gettext, and not in the actual code. We still make
# sure it is legal C code.

for n in redux.??.json
do
    jq '.required_attributes[].label' < $n | awk '{print "_(" $0 "),"}' >> prep.h
done

jq '.countries[].continent' < redux.countries.json | awk '{print "_(" $0 "),"}' >> prep.h

jq '.countries[].name' < redux.countries.json | awk '{print "_(" $0 "),"}' >> prep.h

echo "// This is a generated file, see extract.sh" > extracted.h
echo "const char * i18n[] = {" >> extracted.h
sort prep.h | uniq >> extracted.h
echo "};" >> extracted.h

rm -f prep.h
