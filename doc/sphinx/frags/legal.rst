..
  This file is part of GNU TALER.

  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff


The service has well-known API endpoints to return its legal conditions to the
user in various languages and various formats.  This section describes how to
setup and configure the legal conditions.


Terms of Service
----------------

The service has an endpoint "/terms" to return the terms of service (in legal
language) of the service operator.  Client software show these terms of
service to the user when the user is first interacting with the service.
Terms of service are optional for experimental deployments, if none are
configured, the service will return a simple statement saying that there are
no terms of service available.

To configure the terms of service response, there are two options
in the configuration file for the service:

-   ``TERMS_ETAG``: The current "Etag" to return for the terms of service.
    This value must be changed whenever the terms of service are
    updated. A common value to use would be a version number.
    Note that if you change the ``TERMS_ETAG``, you MUST also provide
    the respective files in ``TERMS_DIR`` (see below).
-   ``TERMS_DIR``: The directory that contains the terms of service.
    The files in the directory must be readable to the service
    process.


Privacy Policy
--------------

The service has an endpoint "/pp" to return the terms privacy policy (in legal
language) of the service operator.  Clients should show the privacy policy to
the user when the user explicitly asks for it, but it should not be shown by
default.  Privacy policies are optional for experimental deployments, if none
are configured, the service will return a simple statement saying that there
is no privacy policy available.

To configure the privacy policy response, there are two options
in the configuration file for the service:

-   ``PRIVACY_ETAG``: The current "Etag" to return for the privacy policy.
    This value must be changed whenever the privacy policy is
    updated. A common value to use would be a version number.
    Note that if you change the ``PRIVACY_ETAG``, you MUST also provide
    the respective files in ``PRIVACY_DIR`` (see below).
-   ``PRIVACY_DIR``: The directory that contains the privacy policy.
    The files in the directory must be readable to the service
    process.


Legal policies directory layout
-------------------------------

The ``TERMS_DIR`` and ``PRIVACY_DIR`` directory structures must follow a
particular layout.  You may use the same directory for both the terms of
service and the privacy policy, as long as you use different ETAGs.  Inside of
the directory, there should be sub-directories using two-letter language codes
like "en", "de", or "jp".  Each of these directories would then hold
translations of the current terms of service into the respective language.
Empty directories are permitted in case translations are not available.

Then, inside each language directory, files with the name of the value set as
the ``TERMS_ETAG`` or ``PRIVACY_ETAG`` must be provided. The extension of each
of the files should be typical for the respective mime type.  The set of
supported mime types is currently hard-coded in the service, and includes
".epub", ".html", ".md", ".pdf" and ".txt" files. If other files are present,
the service may show a warning on startup.

Example
^^^^^^^

A sample file structure for a ``TERMS_ETAG`` of "tos-v0" would be:

-   TERMS_DIR/en/tos-v0.txt
-   TERMS_DIR/en/tos-v0.html
-   TERMS_DIR/en/tos-v0.pdf
-   TERMS_DIR/en/tos-v0.epub
-   TERMS_DIR/en/tos-v0.md
-   TERMS_DIR/de/tos-v0.txt
-   TERMS_DIR/de/tos-v0.html
-   TERMS_DIR/de/tos-v0.pdf
-   TERMS_DIR/de/tos-v0.epub
-   TERMS_DIR/de/tos-v0.md

If the user requests an HTML format with language preferences "fr" followed by
"en", the service would return ``TERMS_DIR/en/tos-v0.html`` lacking a version in
French.


Generating the Legal Terms
--------------------------

The ``taler-terms-generator`` script can be used to generate directories with
terms of service and privacy policies in multiple languages and all required
data formats from a single source file in ``.rst`` format and GNU gettext
translations in ``.po`` format.

To use the tool, you need to first write your legal conditions in English in
reStructuredText (rst).  You should find a templates in
``$PREFIX/share/terms/*.rst`` where ``$PREFIX`` is the location where you
installed the service to. Whenever you make substantive changes to the legal
terms, you must use a fresh filename and change the respective ``ETAG``.  The
resulting file must be called ``$ETAG.rst`` and the first line of the file should be the title of the document.

Once you have written the ``$ETAG.rst`` file in English, you can
generate the first set of outputs:

.. code-block:: shell-session

    $ taler-terms-generator -i $ETAG

Afterwards, you should find the terms in various formats for all configured
languages (initially only English) in ``$PREFIX/share/terms/``.  The generator
has a few options which are documented in its man page.


Adding translations
-------------------

Translations must be available in subdirectories
``locale/$LANGUAGE/LC_MESSAGES/$ETAG.po``.
To start translating, you first need to add a new
language:

.. code-block:: shell-session

    $ taler-terms-generator -i $ETAG -l $LANGUAGE

Here, ``$LANGUAGE`` should be a two-letter language
code like ``de`` or ``fr``.  The command will generate
a file ``locale/$LANGUAGE/LC_MESSAGES/$ETAG.po``
which contains each English sentence or paragraph
in the original document and an initially empty
translation.  Translators should update the ``.po``
file. Afterwards, simply re-run

.. code-block:: shell-session

    $ taler-terms-generator -i $ETAG

to make the current translation(s) available to the
service.

.. note::

   You must restart the service whenever adding or updating legal documents or their translations.


Updating legal documents
------------------------

When making minor changes without legal implications, edit the ``.rst`` file,
then re-run the step to add a new language for each existing translation to
produce an updated ``.po`` file. Translate the sentences that have changed and
finally run the generator (without ``-l``) on the ETAG (``-i $ETAG``) to
create the final files.

When making major changes with legal implications, you should first rename (or
copy) the existing ``.rst`` file and the associated translation files to a new
unique name.  Afterwards, make the major changes, update the ``.po`` files,
complete the translations and re-create the final files.  Finally, do not
forget to update the ``ETAG`` configuration option to the new name and to
restart the service.
