anastasis-gtk(1)
################

.. only:: html

   Name
   ====

   **anastasis-gtk** - Gtk+ GUI for Anastasis

Synopsis
========

**anastasis-gtk**
[**-A**_*ID*_|_**--application=**\ \ *ID*]
[**-c** *FILENAME* | **––config=**\ ‌\ *FILENAME*]
[**-h** | **––help**]
[**-L** *LOGLEVEL* | **––loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **––logfile=**\ ‌\ *FILENAME*]
[**-v** | **––version**]


Description
===========

**anastasis-gtk** is a graphical tool to run Anastasis
key recover and backup operations.


**-A** *ID* \| **--application=**\ \ *ID*
   Set the application ID to *ID*. Default is ``anastasis-gtk``. Used
   to store different types of secrets from different applications
   while using the same user attributes. Basically the application ID
   is included in the user attributes. Not changable by the GUI as
   only advanced users should even known about this. Applications that
   tightly integrate Anastasis should set the application ID to their
   respective unique name, for example the GNU Taler wallet may use
   ``gnu-taler-wallet`` for the application ID. If anastasis-gtk is
   to be used to recover such a secret, the respective application ID
   must be provided on the command-line. Users that only use
   anastasis-gtk to backup and restore secrets should not set the
   application ID, as forgetting the ID makes the secrets irrecoverable.

**-c** *FILENAME* \| **––config=**\ ‌\ *FILENAME*
   Use the configuration from *FILENAME*.

**-h** \| **––help**
   Print short help on options.

**-L** *LOGLEVEL* \| **––loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **––logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-v** \| **––version**
   Print version information.

See Also
========

anastasis-reducer(1), anastasis-httpd(1), anastasis.conf(5).

Bugs
====

Report bugs by using https://bugs.anastasis.lu/ or by sending electronic
mail to <contact@anastasis.lu>.
