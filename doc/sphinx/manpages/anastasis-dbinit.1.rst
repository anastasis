anastasis-dbinit(1)
###################

.. only:: html

   Name
   ====

   **anastasis-dbinit** - initialize Anastasis database


Synopsis
========

**anastasis-dbinit**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-g** | **--gc**]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-r** | **--reset**]
[**-v** | **--version**]

Description
===========

**anastasis-dbinit** is a command-line tool to initialize the GNU
Anastasis database. It creates the necessary tables and indices for
an Anastasis server to operate.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-g** \| **--gc**
   Garbage collect database. Deletes all unnecessary data in the
   database.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-r** \| **--reset**
   Drop tables. Dangerous, will delete all existing data in the database
   before creating the tables.

**-v** \| **–version**
   Print version information.

See Also
========

anastasis-httpd(1), anastasis.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
