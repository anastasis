/*
  This file is part of Anastasis
  Copyright (C) 2020, 2021 Anastasis SARL

  Anastasis is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Anastasis is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Anastasis; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file reducer/validation_NL_BSN.c
 * @brief Validation for Dutch Buergerservicenummern
 * @author Christian Grothoff
 */
#include <string.h>
#include <stdbool.h>


/**
 * Function to validate a Dutch Social Security number.
 *
 * See https://nl.wikipedia.org/wiki/Burgerservicenummer
 *
 * @param bsn_number social security number to validate (input)
 * @return true if validation passed, else false
 */
bool
NL_BSN_check (const char *bsn_number);

/* declaration to fix compiler warning */
bool
NL_BSN_check (const char *bsn_number)
{
  static const int factors[] = {
    9, 8, 7, 6, 5, 4, 3, 2, -1
  };
  unsigned int sum = 0;

  if (strlen (bsn_number) != 9)
    return false;
  for (unsigned int i = 0; i<8; i++)
  {
    unsigned char c = (unsigned char) bsn_number[i];

    if ( ('0' > c) || ('9' < c) )
      return false;
    sum += (c - '0') * factors[i];
  }
  {
    unsigned char c = (unsigned char) bsn_number[8];
    unsigned int v = (c - '0');

    return (sum % 11 == v);
  }
}
