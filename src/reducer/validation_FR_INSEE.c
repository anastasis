/*
  This file is part of Anastasis
  Copyright (C) 2022 Anastasis SARL

  Anastasis is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Anastasis is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Anastasis; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file reducer/validation_FR_INSEE.c
 * @brief Validation for French INSEE Numbers
 * @author Christian Grothoff
 */
#include <string.h>
#include <stdbool.h>
#include <stdio.h>


/**
 * Function to validate a French INSEE number.
 *
 * See https://en.wikipedia.org/wiki/INSEE_code
 *
 * Note that we do not implement checks on the month
 * and also allow non-binary prefixes.
 *
 * @param insee_number social security number to validate (input)
 * @return true if validation passed, else false
 */
bool
FR_INSEE_check (const char *insee_number);

/* declaration to fix compiler warning */
bool
FR_INSEE_check (const char *insee_number)
{
  char pfx[14];
  unsigned long long num;
  unsigned int cc;
  char dum;

  if (strlen (insee_number) != 15)
    return false;
  memcpy (pfx,
          insee_number,
          13);
  pfx[13] = '\0';
  if (1 !=
      sscanf (pfx,
              "%llu%c",
              &num,
              &dum))
    return false;
  if (1 !=
      sscanf (&insee_number[13],
              "%u%c",
              &cc,
              &dum))
    return false;
  if (97 - cc != num % 97)
    return false;
  return true;
}
