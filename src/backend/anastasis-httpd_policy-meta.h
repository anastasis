/*
  This file is part of Anastasis
  Copyright (C) 2022 Anastasis SARL

  Anastasis is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Anastasis is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Anastasis; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file anastasis-httpd_policy-meta.h
 * @brief functions to handle incoming requests on /policy/
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Christian Grothoff
 */
#ifndef ANASTASIS_HTTPD_POLICY_META_H
#define ANASTASIS_HTTPD_POLICY_META_H
#include <microhttpd.h>


/**
 * Handle GET /policy/$ACCOUNT_PUB/meta request.
 *
 * @param connection the MHD connection to handle
 * @param account_pub public key of the account
 * @return MHD result code
 */
MHD_RESULT
AH_policy_meta_get (
  struct MHD_Connection *connection,
  const struct ANASTASIS_CRYPTO_AccountPublicKeyP *account_pub);


#endif
