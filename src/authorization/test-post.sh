#!/bin/bash
# This file is in the public domain.
set -eu
ADDR=`jq -n '{
        full_name: "John Doe",
        street: "Bar street 3",
        city: "Wuppertal",
        postcode: 42289,
        country: "DE",
    }'`

echo "Your recovery code is 1234" | ./anastasis-authorization-post.sh "$ADDR"
