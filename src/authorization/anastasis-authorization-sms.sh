#!/bin/bash
# This file is in the public domain.
set -eu

# Check shared secrets
if [ -x "$TELESIGN_AUTH_TOKEN" ]
then
    echo "TELESIGN_AUTH_TOKEN not sent in environment"
    exit 1
fi

MESSAGE=$(cat -)
TMPFILE=$(mktemp /tmp/sms-loggingXXXXXX)
STATUS=$(curl --request POST \
     --url https://rest-api.telesign.com/v1/messaging \
     --header 'authorization: Basic $TELESIGN_AUTH_TOKEN' \
     --header 'content-type: application/x-www-form-urlencoded' \
     --data account_livecycle_event=transact \
     --data "message=$MESSAGE" \
     --data message_type=OTP \
     --data "phone_number=$1" \
     -w "%{http_code}" -s -o $TMPFILE)
case $STATUS in
    200|203|250|290|291|295)
        exit 0;
        ;;
    *)
        exit 1;
        ;;
esac
exit 1
