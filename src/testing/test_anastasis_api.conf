# This file is in the public domain.
#
[PATHS]
TALER_TEST_HOME = test_anastasis_api_home/
TALER_HOME = ${TALER_TEST_HOME:-${HOME:-${USERPROFILE}}}
TALER_DATA_HOME = ${TALER_TEST_HOME:-${XDG_DATA_HOME:-${TALER_HOME}/.local/share/}/.local/share/}taler/
TALER_CONFIG_HOME = ${TALER_TEST_HOME:-${XDG_CONFIG_HOME:-${TALER_HOME}/.config/}/.config/}taler/
TALER_CACHE_HOME = ${TALER_TEST_HOME:-${XDG_CACHE_HOME:-${TALER_HOME}/.cache/}/.cache/}taler/
TALER_RUNTIME_DIR = ${TMPDIR:-${TMP:-/tmp}}/taler-system-runtime/

[taler-helper-crypto-rsa]
LOOKAHEAD_SIGN = 12 days

[taler-helper-crypto-eddsa]
LOOKAHEAD_SIGN = 12 days
DURATION = 7 days

[bank]
HTTP_PORT = 8082
BASE_URL = http://localhost:8082/

[libeufin-bank]
CURRENCY = EUR
WIRE_TYPE = iban
IBAN_PAYTO_BIC = SANDBOXX
DEFAULT_CUSTOMER_DEBT_LIMIT = EUR:200
DEFAULT_ADMIN_DEBT_LIMIT = EUR:2000
REGISTRATION_BONUS_ENABLED = yes
REGISTRATION_BONUS = EUR:100
SUGGESTED_WITHDRAWAL_EXCHANGE = http://localhost:8081/
SERVE = tcp
PORT = 8082

[anastasis]
PORT = 8086
DB = postgres
CURRENCY = EUR
BUSINESS_NAME = "Checker's Test Inc."
UPLOAD_LIMIT_MB = 1
ANNUAL_POLICY_UPLOAD_LIMIT = 64
INSURANCE = EUR:0
PROVIDER_SALT = salty
ANNUAL_FEE = EUR:4.99
TRUTH_UPLOAD_FEE = EUR:0.0
BASE_URL = http://localhost:8086/

[anastasis-merchant-backend]
PAYMENT_BACKEND_URL = http://localhost:8080/

[authorization-question]
CURRENCY = EUR
COST = EUR:0

[authorization-file]
CURRENCY = EUR
COST = EUR:1

[authorization-email]
CURRENCY = EUR
COST = EUR:0

[authorization-sms]
CURRENCY = EUR
COST = EUR:0
COMMAND = ./sms_authentication.sh

[stasis-postgres]
CONFIG = postgres:///anastasischeck

[merchant]
PORT = 8080
WIRE_TRANSFER_DELAY = 0 s
DB = postgres

[merchantdb-postgres]
CONFIG = postgres:///talercheck

[merchant-exchange-default]
MASTER_KEY = T1VVFQZZARQ1CMF4BN58EE7SKTW5AV2BS18S87ZEGYS4S29J6DNG
EXCHANGE_BASE_URL = http://localhost:8081/
CURRENCY = EUR

[auditor]
PORT = 8083
BASE_URL = "http://localhost:8083/"

[exchange]
CURRENCY = EUR
CURRENCY_ROUND_UNIT = EUR:0.01
DB = postgres
PORT = 8081
SIGNKEY_LEGAL_DURATION = 2 years
MASTER_PUBLIC_KEY = T1VVFQZZARQ1CMF4BN58EE7SKTW5AV2BS18S87ZEGYS4S29J6DNG
BASE_URL = "http://localhost:8081/"
SERVE = tcp
STEFAN_ABS = "EUR:5"

[exchange-offline]
MASTER_PRIV_FILE = ${TALER_DATA_HOME}/exchange/offline-keys/master.priv

SECM_TOFU_FILE = ${TALER_DATA_HOME}/exchange/offline-keys/secm_tofus.pub

[taler-exchange-secmod-eddsa]
KEY_DIR = ${TALER_DATA_HOME}/exchange-secmod-eddsa/keys

[taler-exchange-secmod-rsa]
KEY_DIR = ${TALER_DATA_HOME}/exchange-secmod-rsa/keys

[taler-exchange-secmod-cs]
KEY_DIR = ${TALER_DATA_HOME}/exchange-secmod-cs/keys


[exchangedb-postgres]
CONFIG = "postgres:///talercheck"

[exchange-account-exchange]
PAYTO_URI = "payto://x-taler-bank/localhost:8082/2?receiver-name=exchange"
ENABLE_DEBIT = YES
ENABLE_CREDIT = YES

[exchange-accountcredentials-exchange]
WIRE_GATEWAY_URL = "http://localhost:8082/accounts/2/taler-wire-gateway/"
WIRE_GATEWAY_AUTH_METHOD = NONE

[admin-accountcredentials-exchange]
WIRE_GATEWAY_URL = "http://localhost:8082/accounts/2/taler-wire-gateway/"
WIRE_GATEWAY_AUTH_METHOD = NONE

[coin_eur_ct_1]
value = EUR:0.01
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.00
fee_deposit = EUR:0.00
fee_refresh = EUR:0.01
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = RSA

[coin_eur_ct_10]
value = EUR:0.10
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = RSA

[coin_eur_1]
value = EUR:1
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = RSA

[coin_eur_5]
value = EUR:5
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = RSA
